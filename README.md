# UberTesters

Welcome UberTester Client!

This is designed to be a basic file repository containing the media proof of beta-testing activity.

Its Broken Down as follows:

UberTesters                                 - Main repository
           /TeamAmaze                       - Subrepository
                     /<App of your choice>  - the app of interest

Each App of your choice contains one or more media files 
indexed by bug # listed on in the report sheet.

Any questions or missing files, 
   contact me at edwardsb1970@tutanota.com

--Edward S.B.
Contract Beta-Tester For Hire
